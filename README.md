# chessboard-importer-backend

REST API written in Fast API used to recognize images taken with mobile application.

---
## Run app in a container - Linux
1. Install Docker & Docker-Compose
2. Create Docker group in order not to use 'sudo' command **[link](https://docs.docker.com/engine/install/linux-postinstall/)**
3. To start server run the following commands in directory containing docker-compose.yml:
```shell 
foo@bar:~$ docker-compose up
```
4. To stop server:
```shell 
foo@bar:~$ ^C
foo@bar:~$ docker-compose down
```
Important! Always stop this way.

## Add new packages
1. Install and configure Poetry
2. In directory containing pyproject.toml. Example add:
```shell 
foo@bar:~$ poetry add numpy
```
3. Example remove:
```shell 
foo@bar:~$ poetry remove numpy
```