from fastapi import FastAPI, File, UploadFile
from io import BytesIO
from PIL import Image 
import uvicorn


app = FastAPI()

def read_image(file) -> Image.Image:
    image = Image.open(BytesIO(file))
    return image


@app.post("/photo")
async def return_fen(file: UploadFile = File(...)):
    image = read_image(await file.read())
    width,height=image.size
    #For test POST will answear with width and height
    #Later change for FEN return
    return{"width":width, "height":height}


@app.get("/")
def read_root():
    return {"Hello": "World"}


if __name__== "__main__":
    uvicorn.run(app)
